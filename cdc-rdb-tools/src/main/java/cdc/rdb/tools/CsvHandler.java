package cdc.rdb.tools;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import cdc.office.csv.CsvWriter;

class CsvHandler implements AutoCloseable {
    private final CsvWriter writer;
    private int count = 0;

    public CsvHandler(File file) throws IOException {
        this.writer = new CsvWriter(file);
    }

    public void add(ResultSet rs) throws SQLException, IOException {
        if (count == 0) {
            for (int index = 1; index <= rs.getMetaData().getColumnCount(); index++) {
                writer.write(rs.getMetaData().getColumnName(index) + "(" + rs.getMetaData().getColumnTypeName(index) + ")");
            }
            writer.writeln();
        }
        count++;
        for (int index = 1; index <= rs.getMetaData().getColumnCount(); index++) {
            final Object value = rs.getObject(index);
            writer.write(value == null ? "" : value.toString());
        }
        writer.writeln();
    }

    @Override
    public void close() throws IOException {
        writer.close();
    }
}