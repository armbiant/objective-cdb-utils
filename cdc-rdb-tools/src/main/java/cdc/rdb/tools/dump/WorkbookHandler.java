package cdc.rdb.tools.dump;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.office.ss.WorkbookKind;
import cdc.office.ss.WorkbookWriter;
import cdc.office.ss.WorkbookWriterFactory;
import cdc.office.ss.WorkbookWriterFeatures;
import cdc.office.tables.TableSection;
import cdc.rdb.tools.dump.RdbDump.MainArgs;

final class WorkbookHandler extends AbstractHandler {
    private static final Logger LOGGER = LogManager.getLogger(WorkbookHandler.class);
    private final WorkbookKind kind;
    private WorkbookWriter<?> writer = null;

    public WorkbookHandler(MainArgs margs,
                           WorkbookKind kind) {
        super(margs);
        this.kind = kind;
    }

    @Override
    public String getExtension() {
        return kind.getExtension();
    }

    @Override
    public void startTable(String basename,
                           String tableName) throws IOException {
        final File file = getFile(basename);
        if (margs.isEnabled(MainArgs.Feature.VERBOSE)) {
            LOGGER.info("Create {}", file);
        }
        final WorkbookWriterFactory factory = new WorkbookWriterFactory();
        writer = factory.create(file, WorkbookWriterFeatures.STANDARD_BEST);
        writer.beginSheet(tableName);
    }

    @Override
    public void header(List<String> columnNames) throws IOException {
        writer.beginRow(TableSection.HEADER);
        writer.addCells(columnNames);
    }

    @Override
    public void startRow(int rowIndex) throws IOException {
        writer.beginRow(TableSection.DATA);
    }

    @Override
    public void column(String name,
                       String value) throws IOException {
        writer.addCell(value);
    }

    @Override
    public void endRow() {
        // Ignore
    }

    @Override
    public void endTable() throws IOException {
        writer.close();
    }
}