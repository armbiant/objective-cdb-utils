package cdc.rdb.tools.dump.config;

public enum Processing {
    INHERIT,
    KEEP,
    IGNORE
}