package cdc.rdb.tools.dump.config;

import cdc.converters.Converter;

/**
 * Column configuration.
 *
 * @author Damien Carbonne
 *
 */
public final class ColumnConfig extends AbstractNamedConfig {
    private Converter<?, ?> converter = null;

    ColumnConfig(Builder builder) {
        super(builder);
        this.converter = builder.converter;
    }

    ColumnConfig(TableConfig parent,
                 String internal) {
        super(parent, internal);
        setProcessing(Processing.INHERIT);
    }

    @Override
    public TableConfig getParent() {
        return (TableConfig) super.getParent();
    }

    public Converter<?, ?> getConverter() {
        return converter;
    }

    public void setConverter(Converter<?, ?> converter) {
        this.converter = converter;
    }

    @Override
    public EffectiveProcessing getEffectiveProcessing() {
        return getInheritedProcessing();
    }

    public static class Builder extends AbstractNamedConfig.Builder<ColumnConfig, Builder> {
        private Converter<?, ?> converter = null;

        protected Builder(TableConfig parent) {
            super(parent);
            processing(Processing.INHERIT);
        }

        @Override
        protected Builder self() {
            return this;
        }

        public Builder converter(Converter<?, ?> converter) {
            this.converter = converter;
            return self();
        }

        @Override
        public ColumnConfig build() {
            return ((TableConfig) parent).register(new ColumnConfig(this));
        }

        public TableConfig back() {
            build();
            return (TableConfig) parent;
        }

    }
}