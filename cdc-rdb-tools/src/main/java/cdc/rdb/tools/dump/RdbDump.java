package cdc.rdb.tools.dump;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.converters.Converter;
import cdc.converters.io.ConvertersIo;
import cdc.office.ss.WorkbookKind;
import cdc.rdb.RdbHelper;
import cdc.rdb.SqlDataType;
import cdc.rdb.tools.Config;
import cdc.rdb.tools.dump.RdbDump.MainArgs.Feature;
import cdc.rdb.tools.dump.config.ColumnConfig;
import cdc.rdb.tools.dump.config.DatabaseConfig;
import cdc.rdb.tools.dump.config.DatabaseConfigIo;
import cdc.rdb.tools.dump.config.Processing;
import cdc.rdb.tools.dump.config.SchemaConfig;
import cdc.rdb.tools.dump.config.TableConfig;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.OptionEnum;
import cdc.util.debug.Debug;
import cdc.util.lang.FailureReaction;
import cdc.util.time.Chronometer;

/**
 * Basic utility used to dump database tables to CSV or XML.
 *
 * @author Damien Carbonne
 *
 */
public final class RdbDump {
    private static final Logger LOGGER = LogManager.getLogger(RdbDump.class);

    private static final Pattern NON_UNICODE = Pattern.compile(".*\\ufffd.*");

    private final MainArgs margs;
    private final Connection connection;
    private final DatabaseMetaData metadata;
    private final RdbHelper helper;

    /** List of active handlers used to dump tables. */
    private final List<AbstractHandler> handlers = new ArrayList<>();

    /** Optional Input configuration to use to dump the database. */
    private DatabaseConfig config = null;
    /** Optional Output configuration to generate for the database. */
    private DatabaseConfig configInit = null;

    public static class MainArgs {
        public enum Feature implements OptionEnum {
            XML("xml", "Generate XML files."),
            CSV("csv", "Generates CSV files."),
            XLSX("xlsx", "Generates XLSX files."),
            ODS("ods", "Generates ODS files."),
            CONFIG_INIT("config-init", "Generates a config file."),
            NO_EMPTY_VALUES("no-empty-values", "Do not generate empty values. XML."),
            GENERIC_ELEMENTS("generic-elements", "Use generic elements for columns. XML."),
            SPECIFIC_ELEMENTS("specific-elements", "Use specific elements for columns (default). XML."),
            SPECIFIC_ATTRIBUTES("specific-attributes", "Use specific attributes for columns. XML."),
            VERBOSE("verbose", "Be verbose.");

            private final String name;
            private final String description;

            private Feature(String name,
                            String description) {
                this.name = name;
                this.description = description;
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }

        /** Database url. */
        public String url;
        /** User name. */
        public String user;
        /** User password. */
        public String password;
        /** JDBC driver. */
        public String driver;
        /** Set of schemas for which data must be collected. */
        public final Set<String> schemas = new HashSet<>();
        /** Output dir. */
        public File outputDir;
        /** prefix */
        public String prefix;
        /** Optional configuration file. */
        public File configFile;
        /** Optional converters file. */
        public File convertersFile;

        protected final FeatureMask<Feature> features = new FeatureMask<>();

        public final void setEnabled(Feature feature,
                                     boolean enabled) {
            features.setEnabled(feature, enabled);
        }

        public final boolean isEnabled(Feature feature) {
            return features.isEnabled(feature);
        }

        public void validate() throws ParseException {
            if (!isEnabled(Feature.CSV)
                    && !isEnabled(Feature.XLSX)
                    && !isEnabled(Feature.XML)
                    && !isEnabled(Feature.ODS)
                    && !isEnabled(Feature.CONFIG_INIT)) {
                throw new ParseException("At least one output format or config init must be specified.");
            }
        }
    }

    private RdbDump(MainArgs margs) throws SQLException {
        this.margs = margs;
        log("Connect to: " + margs.url + " as: " + margs.user);

        if (margs.driver != null) {
            try {
                log("Load driver: " + margs.driver);
                Class.forName(margs.driver);
            } catch (final ClassNotFoundException e) {
                LOGGER.error("Failed to load driver class: {} {}", margs.driver, e.getMessage());
                Debug.printClassPath(LOGGER, Level.ERROR);
            }
        }

        // If converters file is defined, load it
        if (margs.convertersFile != null) {
            try {
                log("Load converters: " + margs.convertersFile);
                final ConvertersIo.DataLoader loader = new ConvertersIo.DataLoader(FailureReaction.WARN);
                loader.loadXml(margs.convertersFile);
            } catch (final IOException e) {
                LOGGER.error("Failed to load converters: {} {}", margs.convertersFile, e.getMessage());
            }
        }

        // If config initialization is wanted, create a Config
        if (margs.isEnabled(Feature.CONFIG_INIT)) {
            this.configInit = new DatabaseConfig();
        }

        // If config is defined, load it
        if (margs.configFile != null) {
            try {
                log("Load config: " + margs.configFile);
                final DatabaseConfigIo.DataLoader loader = new DatabaseConfigIo.DataLoader(FailureReaction.WARN);
                this.config = loader.loadXml(margs.configFile);
            } catch (final IOException e) {
                LOGGER.error("Failed to load config file: {} {}", margs.configFile, e.getMessage());
            }
        }

        if (this.config == null) {
            this.config = new DatabaseConfig();
        }

        if (margs.schemas.isEmpty()) {
            // No schemas are listed: keep them all
            if (margs.configFile == null) {
                this.config.setProcessing(Processing.KEEP);
            }
        } else {
            // Keep only listed schemas
            this.config.setProcessing(Processing.IGNORE);
            // All listed schemas are kept
            // Note: this may contradict what was declared in config file
            for (final String schemaName : margs.schemas) {
                final SchemaConfig schemaConfig = this.config.getOrCreateSchemaConfig(schemaName);
                schemaConfig.setProcessing(Processing.KEEP);
            }
            // All non listed schemas that were loaded are ignored
            for (final String schemaName : config.getSchemaNames()) {
                if (!margs.schemas.contains(schemaName)) {
                    final SchemaConfig schemaConfig = this.config.getOrCreateSchemaConfig(schemaName);
                    schemaConfig.setProcessing(Processing.IGNORE);
                }
            }
        }

        this.connection = DriverManager.getConnection(margs.url, margs.user, margs.password);
        this.metadata = connection.getMetaData();
        this.helper = new RdbHelper(metadata);

        if (margs.isEnabled(MainArgs.Feature.CSV)) {
            handlers.add(new WorkbookHandler(margs, WorkbookKind.CSV));
        }
        if (margs.isEnabled(MainArgs.Feature.XLSX)) {
            handlers.add(new WorkbookHandler(margs, WorkbookKind.XLSX));
        }
        if (margs.isEnabled(MainArgs.Feature.ODS)) {
            handlers.add(new WorkbookHandler(margs, WorkbookKind.ODS));
        }
        if (margs.isEnabled(MainArgs.Feature.XML)) {
            handlers.add(new XmlHandler(margs));
        }
    }

    /**
     * Description of a table that can be dumped.
     *
     * @author Damien Carbonne
     *
     */
    private class Table {
        /**
         * Description of a column.
         */
        private class Column {
            /** Column name. */
            final String name;
            /** Column index in the ResultSet. */
            private final int index;
            /** Column SQL data type. */
            final SqlDataType type;

            public Column(String name,
                          int index,
                          SqlDataType type) {
                this.name = name;
                this.index = index;
                this.type = type;
            }

            public String getValue(ResultSet rs) throws SQLException {
                if (getConverter() == null) {
                    final String value = rs.getString(index);
                    if (LOGGER.isWarnEnabled() && value != null && NON_UNICODE.matcher(value).matches()) {
                        LOGGER.warn("data has non unicode: '{}'", value);
                        try {
                            LOGGER.warn("{}", rs.getBinaryStream(index));
                        } catch (final SQLException e) {
                            LOGGER.error(e);
                        }
                    }
                    return value;
                } else {
                    return (String) getConverter().applyRaw(rs.getObject(index));
                }
            }

            public String getExternalName() {
                return config.getExternalColumnName(schemaName, tableName, name);
            }

            public boolean isAccepted() {
                return config.acceptsColumn(schemaName, tableName, name);
            }

            public Converter<?, ?> getConverter() {
                return config.getConverter(schemaName, tableName, name);
            }

            @Override
            public String toString() {
                return "[" + name + " " + index + " " + type + "]";
            }
        }

        final String catalogName;
        final String schemaName;
        final String tableName;
        final List<Column> columns = new ArrayList<>();

        public Table(String catalogName,
                     String schemaName,
                     String internalTableName) {
            this.catalogName = catalogName;
            this.schemaName = schemaName;
            this.tableName = internalTableName;
        }

        private boolean isAccepted() {
            return config.acceptsTable(schemaName, tableName);
        }

        private String getExternalSchemaName() {
            return config.getExternalSchemaName(schemaName);
        }

        private String getExternalTableName() {
            return config.getExternalTableName(schemaName, tableName);
        }

        private String getOrderClause() {
            final TableConfig x = config.getTableConfig(schemaName, tableName);
            if (x == null) {
                return "";
            } else {
                return helper.getOrderClause(x.getColumnsSorting());
            }
        }

        private String getBaseName() {
            final String sep = "-";
            final StringBuilder builder = new StringBuilder();
            if (catalogName != null && !catalogName.isEmpty()) {
                builder.append(catalogName);
                builder.append(sep);
            }
            builder.append(getExternalSchemaName());
            builder.append(sep);
            builder.append(getExternalTableName());
            return builder.toString();
        }

        /**
         * Dumps this table using each active handler.
         */
        public void dump() {
            final String query = helper.getSelectClause(schemaName, tableName) + getOrderClause();
            try (final Statement statement = connection.createStatement();
                    final ResultSet rs = statement.executeQuery(query)) {

                final ResultSetMetaData rsmeta = rs.getMetaData();
                initColumns(rsmeta);
                initConfig();

                if (isAccepted()) {
                    try {
                        RdbDump.this.log("Process '" + schemaName + helper.getCatalogSeparator() + tableName + "'");
                        dumpStart();

                        int rowIndex = 0;
                        while (rs.next()) {
                            dumpRow(rs, rowIndex);
                            rowIndex++;
                        }
                    } finally {
                        dumpStop();
                    }
                } else {
                    RdbDump.this.log("Skip '" + schemaName + helper.getCatalogSeparator() + tableName + "'");
                }
            } catch (final Exception e) {
                LOGGER.error("Failed to dump: {}, {}, {} with query: {} {}",
                             catalogName,
                             schemaName,
                             tableName,
                             query,
                             e.getMessage());
            }
        }

        private void initColumns(ResultSetMetaData rsmeta) throws SQLException {
            for (int index = 1; index <= rsmeta.getColumnCount(); index++) {
                final Column column = new Column(rsmeta.getColumnName(index),
                                                 index,
                                                 SqlDataType.decode(rsmeta.getColumnType(index)));
                columns.add(column);
            }
        }

        private void initConfig() {
            if (configInit != null) {
                final SchemaConfig schemaConfig = configInit.getOrCreateSchemaConfig(schemaName);
                final TableConfig tableConfig = schemaConfig.getOrCreateTableConfig(tableName);
                for (final Column column : columns) {
                    final ColumnConfig columnConfig = tableConfig.getOrCreateColumnConfig(column.name);
                    columnConfig.setNote("type: " + column.type);
                }
            }
        }

        /**
         * Starts the dump of (an accepted) table using each active handler.
         *
         * @throws IOException When an IO errors occurs.
         */
        private void dumpStart() throws IOException {
            final List<String> externalColumnNames = new ArrayList<>();
            for (final Column column : columns) {
                if (column.isAccepted()) {
                    externalColumnNames.add(column.getExternalName());
                }
            }

            for (final AbstractHandler handler : handlers) {
                handler.startTable(getBaseName(), tableName);
                handler.header(externalColumnNames);
            }
        }

        /**
         * Dumps a row of (an accepted) table using each active handler.
         *
         * @param rs The ResultSet to use.
         * @param rowIndex The row index.
         * @throws SQLException When a SQL error occurs.
         * @throws IOException When an IO errors occurs.
         */
        private void dumpRow(ResultSet rs,
                             int rowIndex) throws SQLException, IOException {
            for (final AbstractHandler handler : handlers) {
                handler.startRow(rowIndex);
                for (final Column column : columns) {
                    if (column.isAccepted()) {
                        final String value = column.getValue(rs);
                        handler.column(column.getExternalName(), value);
                    }
                }
                handler.endRow();
            }
        }

        /**
         * /**
         * Stops the dump of (an accepted) table.
         */
        private void dumpStop() {
            for (final AbstractHandler handler : handlers) {
                try {
                    handler.endTable();
                } catch (final IOException e) {
                    LOGGER.error("Failed to close file", e);
                }
            }
        }

        @Override
        public String toString() {
            return catalogName + "/" + schemaName + "/" + tableName;
        }
    }

    private void log(String message) {
        if (margs.isEnabled(MainArgs.Feature.VERBOSE)) {
            LOGGER.info(message);
        }
    }

    private void execute() {
        try {
            log("List tables");
            // List of tables to dump
            final List<Table> tables = new ArrayList<>();
            try (final ResultSet rs = metadata.getTables(null, null, null, null)) {
                while (rs.next()) {
                    // 1: table catalog (string, may be null)
                    // 2: table schema (string, may be null)
                    // 3: table name (string)
                    // 4: table type (string)
                    // 5: comments (string)
                    // 6: type catalog
                    // 7: type schema
                    // 8: type name
                    // 9: self referencing column name (string)
                    // 10: ref generation (string)
                    final String schemaName = rs.getString(2); // May be null
                    if (acceptsSchema(schemaName)) {
                        final String catalogName = rs.getString(1); // May be null
                        final String tableName = rs.getString(3);
                        tables.add(new Table(catalogName, schemaName, tableName));
                    }
                }
            } catch (final SQLException e) {
                LOGGER.catching(e);
            }

            // Dump all tables
            log("Dump tables");
            final Chronometer chrono = new Chronometer();
            chrono.start();
            for (final Table table : tables) {
                // log("Process " + table);
                table.dump();
            }
            chrono.suspend();
            log("Done (" + chrono + ")");

            // Save config initialization
            if (configInit != null) {
                final File file = new File(margs.outputDir, (margs.prefix == null ? "" : margs.prefix) + "config-init.xml");
                log("Create " + file);
                try {
                    DatabaseConfigIo.Printer.print(configInit, file);
                    log("Done");
                } catch (final IOException e) {
                    LOGGER.catching(e);
                }
            }
        } finally {
            try {
                connection.close();
            } catch (final SQLException e) {
                LOGGER.error("Failed to close connection", e);
            }
        }
    }

    /**
     * Returns {@code true} when a schema name is accepted.
     * <p>
     * Data are generated for accepted schemas.
     *
     * @param schemaName The schema name.
     * @return {@code true} when schemaName is accepted.
     */
    private boolean acceptsSchema(String schemaName) {
        return config.acceptsSchema(schemaName);
    }

    public static void execute(MainArgs margs) throws SQLException {
        final RdbDump instance = new RdbDump(margs);
        instance.execute();
    }

    public static void main(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        private static final String SCHEMA = "schema";
        private static final String CONFIG = "config";
        private static final String CONVERTERS = "converters";

        public MainSupport() {
            super(RdbDump.class, LOGGER);
        }

        @Override
        protected String getVersion() {
            return Config.VERSION;
        }

        @Override
        protected void addSpecificOptions(Options options) {
            options.addOption(Option.builder()
                                    .longOpt(URL)
                                    .desc("URL to access database.")
                                    .hasArg()
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(DRIVER)
                                    .desc("Optional JDBC Driver class.")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(USER)
                                    .desc("Optional user name.")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder("pwd")
                                    .longOpt(PASSWORD)
                                    .desc("Optional user password.")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(SCHEMA)
                                    .desc("Optional set of schemas that must be analysed. When empty, all schemas are analyzed.\n"
                                            + "Note: this may conflict with configuration file option.")
                                    .hasArgs()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(OUTPUT)
                                    .desc("Output directory.")
                                    .hasArg()
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(PREFIX)
                                    .desc("Optional prefix for file names.")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(CONFIG)
                                    .desc("Optional configuration file.\n"
                                            + "Note: one should not use schema option with this one.")
                                    .hasArg()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(CONVERTERS)
                                    .desc("Optional converters file.")
                                    .hasArg()
                                    .build());

            addNoArgOptions(options, MainArgs.Feature.class);
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();
            margs.url = cl.getOptionValue(URL);
            margs.user = cl.getOptionValue(USER);
            margs.password = cl.getOptionValue(PASSWORD);
            margs.driver = cl.getOptionValue(DRIVER);
            if (cl.hasOption(SCHEMA)) {
                for (final String s : cl.getOptionValues(SCHEMA)) {
                    margs.schemas.add(s);
                }
            }
            margs.outputDir = getValueAsFile(cl, OUTPUT, IS_NULL_OR_DIRECTORY);
            margs.prefix = cl.getOptionValue(PREFIX);
            margs.configFile = getValueAsFile(cl, CONFIG, IS_NULL_OR_FILE);
            margs.convertersFile = getValueAsFile(cl, CONVERTERS, IS_NULL_OR_FILE);

            setMask(cl, MainArgs.Feature.class, margs.features::setEnabled);

            margs.validate();

            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws Exception {
            RdbDump.execute(margs);
            return null;
        }
    }
}