package cdc.rdb.tools.dump.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Schema configuration.
 *
 * @author Damien Carbonne
 *
 */
public final class SchemaConfig extends AbstractNamedConfig {
    private final Map<String, TableConfig> tables = new HashMap<>();

    TableConfig register(TableConfig config) {
        tables.put(config.getInternal(), config);
        return config;
    }

    SchemaConfig(Builder builder) {
        super(builder);
    }

    SchemaConfig(DatabaseConfig parent,
                 String internal) {
        super(parent, internal);
        setProcessing(Processing.INHERIT);
    }

    @Override
    public DatabaseConfig getParent() {
        return (DatabaseConfig) super.getParent();
    }

    @Override
    public EffectiveProcessing getEffectiveProcessing() {
        return toEffectiveProcessing(tables.values());
    }

    public Set<String> getTableNames() {
        return tables.keySet();
    }

    public TableConfig getOrCreateTableConfig(String tableName) {
        return tables.computeIfAbsent(tableName, s -> new TableConfig(this, s));
    }

    /**
     * Returns the configuration associated to a table.
     *
     * @param tableName The internal table name.
     * @return The configuration associated to {@code tableName} or null.
     */
    public TableConfig getTableConfig(String tableName) {
        return tables.get(tableName);
    }

    public TableConfig.Builder table() {
        return new TableConfig.Builder(this);
    }

    public TableConfig.Builder table(String name) {
        return table().internal(name);
    }

    public static class Builder extends AbstractNamedConfig.Builder<SchemaConfig, Builder> {
        protected Builder(DatabaseConfig parent) {
            super(parent);
            processing(Processing.INHERIT);
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        public SchemaConfig build() {
            return ((DatabaseConfig) parent).register(new SchemaConfig(this));
        }

        public DatabaseConfig back() {
            build();
            return (DatabaseConfig) parent;
        }
    }
}