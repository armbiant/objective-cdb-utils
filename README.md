# cdc-rdb

Relational Database utilities based on JDBC.

## Metadata analyzer
**RdbMetaAnalyzer** is a tool that analyzes meta data of a DB.  
It constructs an in-memory representation of those meta data.  
This can be save to or restored from XML.

## Database dump
**RdbDump** is a tool tan can dump tables of a DB to XML or CSV.  
It can be configured to select tables and columns that must be dumped.  
Structure of generated XML can be controlled.
DB names (tables, columns, ...) can be converted.  