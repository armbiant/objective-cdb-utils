package cdc.rdb;

import java.sql.DatabaseMetaData;

public enum ProcedureResultType {
    UNKNOWN,
    RESULT,
    NO_RESULT;

    public static ProcedureResultType decode(short code) {
        switch (code) {
        case DatabaseMetaData.procedureResultUnknown:
            return UNKNOWN;
        case DatabaseMetaData.procedureNoResult:
            return NO_RESULT;
        case DatabaseMetaData.procedureReturnsResult:
            return RESULT;
        default:
            return null;
        }
    }
}