package cdc.rdb;

/**
 * User Data type description.
 * <p>
 * Its parent is a Schema.<br>
 * Its name must be unique.(?)<br>
 * Its content is:
 * <ul>
 * <li>Attributes
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class RdbUserDataType extends RdbElement {
    private String className;
    private SqlDataType type;

    RdbUserDataType(String name,
                    RdbSchema parent) {
        super(name, parent, false);
    }

    @Override
    public final RdbElementKind getKind() {
        return RdbElementKind.USER_DATA_TYPE;
    }

    @Override
    public RdbSchema getParent() {
        return getParent(RdbSchema.class);
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public SqlDataType getType() {
        return type;
    }

    public void setType(SqlDataType type) {
        this.type = type;
    }

    public RdbAttribute createAttribute(String name) {
        return new RdbAttribute(name, this);
    }

    public Iterable<RdbAttribute> getAttributes() {
        return getChildren(RdbAttribute.class);
    }

    public RdbAttribute getOptionalAttribute(String name) {
        return getFirstChild(RdbAttribute.class, name);
    }

    public RdbAttribute getAttribute(String name) {
        return notNull(getOptionalAttribute(name), "attribute", name);
    }
}