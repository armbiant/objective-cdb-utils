package cdc.rdb;

/**
 * Function column description.
 * <p>
 * Its parent is a Function.<br>
 * Its name must be unique.(?)<br>
 * Its is a leaf.
 *
 * @author Damien Carbonne
 *
 */
public final class RdbFunctionColumn extends RdbElement {
    // TODO attributes
    protected RdbFunctionColumn(String name,
                                RdbFunction parent) {
        super(name, parent, false);
    }

    @Override
    public RdbElementKind getKind() {
        return RdbElementKind.FUNCTION_COLUMN;
    }

    @Override
    public RdbFunction getParent() {
        return (RdbFunction) super.getParent();
    }

    public RdbFunction getFunction() {
        return getParent();
    }
}