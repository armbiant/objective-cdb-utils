package cdc.rdb;

/**
 * Data type description.
 * <p>
 * Its parent is a Database.<br>
 * Its name must NOT be unique.<br>
 * Its is a leaf.
 *
 * @author Damien Carbonne
 *
 */
public final class RdbDataType extends RdbElement {
    private final SqlDataType type;
    private int maxPrecision = -1;
    private String literalPrefix;
    private String literalSuffix;
    private String createParams;
    private YesNoUnknown nullable;
    private boolean caseSensitive;
    private boolean unsigned;
    private boolean fixedPrecisionScale;
    private String localizedName;
    private boolean autoIncrement;
    private short minScale;
    private short maxScale;
    private int radix;

    RdbDataType(String name,
                RdbDatabase parent,
                SqlDataType type) {
        super(name, parent, true);
        this.type = type;
    }

    @Override
    public final RdbElementKind getKind() {
        return RdbElementKind.DATA_TYPE;
    }

    @Override
    public RdbDatabase getParent() {
        return getParent(RdbDatabase.class);
    }

    public SqlDataType getType() {
        return type;
    }

    public void setMaxPrecison(int maxPrecision) {
        this.maxPrecision = maxPrecision;
    }

    public int getMaxPrecision() {
        return maxPrecision;
    }

    public String getLiteralPrefix() {
        return literalPrefix;
    }

    public void setLiteralPrefix(String literalPrefix) {
        this.literalPrefix = literalPrefix;
    }

    public String getLiteralSuffix() {
        return literalSuffix;
    }

    public void setLiteralSuffix(String literalSuffix) {
        this.literalSuffix = literalSuffix;
    }

    public String getCreateParams() {
        return createParams;
    }

    public void setCreateParams(String createParams) {
        this.createParams = createParams;
    }

    public YesNoUnknown getNullable() {
        return nullable;
    }

    public void setNullable(YesNoUnknown nullable) {
        this.nullable = nullable;
    }

    public boolean isCaseSensitive() {
        return caseSensitive;
    }

    public void setCaseSensitive(boolean caseSensitive) {
        this.caseSensitive = caseSensitive;
    }

    public boolean isUnsigned() {
        return unsigned;
    }

    public void setUnsigned(boolean unsigned) {
        this.unsigned = unsigned;
    }

    public boolean isFixedPrecisionScale() {
        return fixedPrecisionScale;
    }

    public void setFixedPrecisionScale(boolean fixedPrecisionScale) {
        this.fixedPrecisionScale = fixedPrecisionScale;
    }

    public String getLocalizedName() {
        return localizedName;
    }

    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }

    public boolean isAutoIncrement() {
        return autoIncrement;
    }

    public void setAutoIncrement(boolean autoIncrement) {
        this.autoIncrement = autoIncrement;
    }

    public short getMinScale() {
        return minScale;
    }

    public void setMinScale(short minScale) {
        this.minScale = minScale;
    }

    public short getMaxScale() {
        return maxScale;
    }

    public void setMaxScale(short maxScale) {
        this.maxScale = maxScale;
    }

    public int getRadix() {
        return radix;
    }

    public void setRadix(int radix) {
        this.radix = radix;
    }
}