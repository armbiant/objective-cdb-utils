package cdc.rdb;

import java.sql.RowIdLifetime;
import java.util.EnumMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.Checks;
import cdc.util.lang.FailureReaction;
import cdc.util.strings.CaseConverter;
import cdc.util.strings.Splitter;
import cdc.util.strings.StringConversion;

/**
 * Database description.
 * <p>
 * It is a root element.<br>
 * Its content is:
 * <ul>
 * <li>Properties
 * <li>Catalogs
 * <li>Data types
 * <li>Table types
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class RdbDatabase extends RdbElement {
    private static final Logger LOGGER = LogManager.getLogger(RdbDatabase.class);
    private final Map<EnumProperty, Enum<?>> enumProperties = new EnumMap<>(EnumProperty.class);
    private final Map<StringProperty, String> stringProperties = new EnumMap<>(StringProperty.class);
    private final Map<IntegerProperty, Integer> integerProperties = new EnumMap<>(IntegerProperty.class);
    private final Map<LongProperty, Long> longProperties = new EnumMap<>(LongProperty.class);
    private final Map<BooleanProperty, Boolean> booleanProperties = new EnumMap<>(BooleanProperty.class);
    private RdbIdentifierNormalizer identifierNormalizer = null;

    private static final CaseConverter CONVERTER =
            CaseConverter.builder()
                         .style(CaseConverter.Style.CAPITAL)
                         .splitter(new Splitter('_'))
                         .build();

    public enum PropertyKind {
        ENUM,
        STRING,
        INTEGER,
        LONG,
        BOOLEAN
    }

    /**
     * Enumeration of enum properties.
     *
     * @author Damien Carbonne
     *
     */
    public enum EnumProperty {
        ROW_ID_LIFETIME(RowIdLifetime.class);

        private final Class<? extends Enum<?>> enumClass;

        private EnumProperty(Class<? extends Enum<?>> enumClass) {
            this.enumClass = enumClass;
        }

        public Class<? extends Enum<?>> getEnumClass() {
            return enumClass;
        }

        public String getMethodName() {
            return "get" + CONVERTER.splitAndConvert(name());
        }
    }

    /**
     * Enumeration of string properties.
     *
     * @author Damien Carbonne
     *
     */
    public enum StringProperty {
        CATALOG_SEPARATOR,
        CATALOG_TERM,
        DATABASE_PRODUCT_NAME,
        DATABASE_PRODUCT_VERSION,
        DRIVER_NAME,
        DRIVER_VERSION,
        EXTRA_NAME_CHARACTERS,
        IDENTIFIER_QUOTE_STRING,
        NUMERIC_FUNCTIONS,
        PROCEDURE_TERM,
        SCHEMA_TERM,
        SEARCH_STRING_ESCAPE,
        SQL_KEYWORDS,
        STRING_FUNCTIONS,
        SYSTEM_FUNCTIONS,
        TIME_DATE_FUNCTIONS,
        URL,
        USER_NAME;

        public String getMethodName() {
            switch (this) {
            case SQL_KEYWORDS:
                return "getSQLKeywords";
            case URL:
                return "getURL";
            default:
                return "get" + CONVERTER.splitAndConvert(name());
            }
        }
    }

    /**
     * Enumeration of integer properties.
     *
     * @author Damien Carbonne
     *
     */
    public enum IntegerProperty {
        SQL_STATE_TYPE, // Enum in fact
        RESULT_SET_HOLDABILITY, // Enum in fact
        DATABASE_MAJOR_VERSION,
        DATABASE_MINOR_VERSION,
        DEFAULT_TRANSACTION_ISOLATION, // Mask
        DRIVER_MAJOR_VERSION,
        DRIVER_MINOR_VERSION,
        JDBC_MAJOR_VERSION,
        JDBC_MINOR_VERSION,
        MAX_BINARY_LITERAL_LENGTH,
        MAX_CATALOG_NAME_LENGTH,
        MAX_CHAR_LITERAL_LENGTH,
        MAX_COLUMN_NAME_LENGTH,
        MAX_COLUMNS_IN_GROUP_BY,
        MAX_COLUMNS_IN_INDEX,
        MAX_COLUMNS_IN_ORDER_BY,
        MAX_COLUMNS_IN_SELECT,
        MAX_COLUMNS_IN_TABLE,
        MAX_CONNECTIONS,
        MAX_CURSOR_NAME_LENGTH,
        MAX_INDEX_LENGTH,
        MAX_PROCEDURE_NAME_LENGTH,
        MAX_ROW_SIZE,
        MAX_SCHEMA_NAME_LENGTH,
        MAX_STATEMENT_LENGTH,
        MAX_STATEMENTS,
        MAX_TABLE_NAME_LENGTH,
        MAX_TABLES_IN_SELECT,
        MAX_USER_NAME_LENGTH;

        public String getMethodName() {
            final String s = "get" + CONVERTER.splitAndConvert(name());
            return s.replace("Sql", "SQL").replace("Jdbc", "JDBC");
        }
    }

    /**
     * Enumeration of long properties.
     *
     * @author Damien Carbonne
     *
     */
    public enum LongProperty {
        MAX_LOGICAL_LOB_SIZE;

        public String getMethodName() {
            return "get" + CONVERTER.splitAndConvert(name());
        }
    }

    /**
     * Enumeration of boolean properties.
     *
     * @author Damien Carbonne
     *
     */
    public enum BooleanProperty {
        ALL_PROCEDURES_ARE_CALLABLE,
        ALL_TABLES_ARE_SELECTABLE,
        AUTO_COMMIT_FAILURE_CLOSES_ALL_RESULT_SETS,
        DATA_DEFINITION_CAUSES_TRANSACTION_COMMIT,
        DATA_DEFINITION_IGNORED_IN_TRANSACTIONS,
        DOES_MAX_ROW_SIZE_INCLUDE_BLOBS,
        GENERATED_KEY_ALWAYS_RETURNED,
        IS_CATALOG_AT_START,
        IS_READ_ONLY,
        LOCATORS_UPDATE_COPY,
        NULL_PLUS_NON_NULL_IS_NULL,
        NULLS_ARE_SORTED_AT_END,
        NULLS_ARE_SORTED_AT_START,
        NULLS_ARE_SORTED_HIGH,
        NULLS_ARE_SORTED_LOW,
        STORES_LOWER_CASE_IDENTIFIERS,
        STORES_LOWER_CASE_QUOTED_IDENTIFIERS,
        STORES_MIXED_CASE_IDENTIFIERS,
        STORES_MIXED_CASE_QUOTED_IDENTIFIERS,
        STORES_UPPER_CASE_IDENTIFIERS,
        STORES_UPPER_CASE_QUOTED_IDENTIFIERS,
        USES_LOCAL_FILE_PER_TABLE,
        USES_LOCAL_FILES,
        SUPPORTS_ALTER_TABLE_WITH_ADD_COLUMN,
        SUPPORTS_ALTER_TABLE_WITH_DROP_COLUMN,
        SUPPORTS_ANSI92_ENTRY_LEVEL_SQL,
        SUPPORTS_ANSI92_FULL_SQL,
        SUPPORTS_ANSI92_INTERMEDIATE_SQL,
        SUPPORTS_BATCH_UPDATES,
        SUPPORTS_CATALOGS_IN_DATA_MANIPULATION,
        SUPPORTS_CATALOGS_IN_INDEX_DEFINITIONS,
        SUPPORTS_CATALOGS_IN_PRIVILEGE_DEFINITIONS,
        SUPPORTS_CATALOGS_IN_PROCEDURE_CALLS,
        SUPPORTS_CATALOGS_IN_TABLE_DEFINITIONS,
        SUPPORTS_COLUMN_ALIASING,
        SUPPORTS_CONVERT,
        SUPPORTS_CORE_SQL_GRAMMAR,
        SUPPORTS_MINIMUM_SQL_GRAMMAR,
        SUPPORTS_EXTENDED_SQL_GRAMMAR,
        SUPPORTS_CORRELATED_SUBQUERIES,
        SUPPORTS_DATA_DEFINITION_AND_DATA_MANIPULATION_TRANSACTIONS,
        SUPPORTS_DATA_MANIPULATION_TRANSACTIONS_ONLY,
        SUPPORTS_DIFFERENT_TABLE_CORRELATION_NAMES,
        SUPPORTS_EXPRESSIONS_IN_ORDER_BY,
        SUPPORTS_FULL_OUTER_JOINS,
        SUPPORTS_GET_GENERATED_KEYS,
        SUPPORTS_GROUP_BY,
        SUPPORTS_GROUP_BY_BEYOND_SELECT,
        SUPPORTS_GROUP_BY_UNRELATED,
        SUPPORTS_INTEGRITY_ENHANCEMENT_FACILITY,
        SUPPORTS_LIKE_ESCAPE_CLAUSE,
        SUPPORTS_MIXED_CASE_IDENTIFIERS,
        SUPPORTS_MIXED_CASE_QUOTED_IDENTIFIERS,
        SUPPORTS_MULTIPLE_OPEN_RESULTS,
        SUPPORTS_MULTIPLE_RESULT_SETS,
        SUPPORTS_MULTIPLE_TRANSACTIONS,
        SUPPORTS_NAMED_PARAMETERS,
        SUPPORTS_NON_NULLABLE_COLUMNS,
        SUPPORTS_OPEN_CURSORS_ACROSS_COMMIT,
        SUPPORTS_OPEN_CURSORS_ACROSS_ROLLBACK,
        SUPPORTS_OPEN_STATEMENTS_ACROSS_COMMIT,
        SUPPORTS_OPEN_STATEMENTS_ACROSS_ROLLBACK,
        SUPPORTS_ORDER_BY_UNRELATED,
        SUPPORTS_LIMITED_OUTER_JOINS,
        SUPPORTS_OUTER_JOINS,
        SUPPORTS_POSITIONED_DELETE,
        SUPPORTS_POSITIONED_UPDATE,
        SUPPORTS_REF_CURSORS,
        SUPPORTS_SAVEPOINTS,
        SUPPORTS_SCHEMAS_IN_DATA_MANIPULATION,
        SUPPORTS_SCHEMAS_IN_INDEX_DEFINITIONS,
        SUPPORTS_SCHEMAS_IN_PRIVILEGE_DEFINITIONS,
        SUPPORTS_SCHEMAS_IN_PROCEDURE_CALLS,
        SUPPORTS_SCHEMAS_IN_TABLE_DEFINITIONS,
        SUPPORTS_SELECT_FOR_UPDATE,
        SUPPORTS_STATEMENT_POOLING,
        SUPPORTS_STORED_FUNCTIONS_USING_CALL_SYNTAX,
        SUPPORTS_STORED_PROCEDURES,
        SUPPORTS_SUBQUERIES_IN_COMPARISONS,
        SUPPORTS_SUBQUERIES_IN_EXISTS,
        SUPPORTS_SUBQUERIES_IN_INS,
        SUPPORTS_SUBQUERIES_IN_QUANTIFIEDS,
        SUPPORTS_TABLE_CORRELATION_NAMES,
        SUPPORTS_TRANSACTIONS,
        SUPPORTS_UNION,
        SUPPORTS_UNION_ALL;

        public String getMethodName() {
            final String s = CONVERTER.splitAndConvert(name());
            final String s1 = Character.toLowerCase(s.charAt(0)) + s.substring(1);
            return s1.replace("Sql", "SQL").replace("Ansi", "ANSI");
        }
    }

    public RdbDatabase(String name) {
        super(name, null, false);
    }

    private void refreshIdentifierNormalizer() {
        identifierNormalizer = RdbIdentifierNormalizer.create(this);
    }

    public RdbIdentifierNormalizer getIdentifierNormalizer() {
        return identifierNormalizer;
    }

    private static <E extends Enum<E>> E toEnum(Class<E> enumClass,
                                                String name) {
        return StringConversion.asOptionalEnum(name, enumClass, null, FailureReaction.DEFAULT);
    }

    public static PropertyKind getPropertyKind(String name) {
        if (toEnum(EnumProperty.class, name) != null) {
            return PropertyKind.ENUM;
        } else if (toEnum(IntegerProperty.class, name) != null) {
            return PropertyKind.INTEGER;
        } else if (toEnum(LongProperty.class, name) != null) {
            return PropertyKind.LONG;
        } else if (toEnum(BooleanProperty.class, name) != null) {
            return PropertyKind.BOOLEAN;
        } else if (toEnum(StringProperty.class, name) != null) {
            return PropertyKind.STRING;
        } else {
            return null;
        }
    }

    @Override
    public RdbElementKind getKind() {
        return RdbElementKind.DATABASE;
    }

    /**
     * Sets an enum property.
     *
     * @param property The property.
     * @param value The property value.
     */
    public void setProperty(EnumProperty property,
                            Enum<?> value) {
        LOGGER.trace("setProperty({}, {})", property, value);
        Checks.isNotNull(property, "property");
        enumProperties.put(property, value);
    }

    /**
     * Returns true if an enum property is defined.
     *
     * @param property The property.
     * @return True if property is defined.
     */
    public boolean isDefined(EnumProperty property) {
        return enumProperties.containsKey(property);
    }

    /**
     * Returns the value of an enum property.
     *
     * @param property The property.
     * @return The value of property.
     * @throws IllegalArgumentException When property is not defined.
     */
    public Enum<?> getProperty(EnumProperty property) {
        Checks.isNotNull(property, "property");
        if (isDefined(property)) {
            return enumProperties.get(property);
        } else {
            throw new IllegalArgumentException("Undefined property: " + property);
        }
    }

    /**
     * Sets a string property.
     *
     * @param property The property.
     * @param value The property value.
     */
    public void setProperty(StringProperty property,
                            String value) {
        LOGGER.trace("setProperty({}, {})", property, value);
        Checks.isNotNull(property, "property");
        stringProperties.put(property, value);
        refreshIdentifierNormalizer();
    }

    /**
     * Returns true if a string property is defined.
     *
     * @param property The property.
     * @return True if property is defined.
     */
    public boolean isDefined(StringProperty property) {
        return stringProperties.containsKey(property);
    }

    /**
     * Returns the value of a string property.
     *
     * @param property The property.
     * @return The value of property.
     * @throws IllegalArgumentException When property is not defined.
     */
    public String getProperty(StringProperty property) {
        Checks.isNotNull(property, "property");
        if (isDefined(property)) {
            return stringProperties.get(property);
        } else {
            throw new IllegalArgumentException("Undefined property: " + property);
        }
    }

    /**
     * Sets an integer property.
     *
     * @param property The property.
     * @param value The property value.
     */
    public void setProperty(IntegerProperty property,
                            int value) {
        LOGGER.trace("setProperty({}, {})", property, value);
        Checks.isNotNull(property, "property");
        integerProperties.put(property, value);
    }

    /**
     * Returns true if an integer property is defined.
     *
     * @param property The property.
     * @return True if property is defined.
     */
    public boolean isDefined(IntegerProperty property) {
        return integerProperties.containsKey(property);
    }

    /**
     * Returns the value of an integer property.
     *
     * @param property The property.
     * @return The value of property.
     * @throws IllegalArgumentException When property is not defined.
     */
    public int getProperty(IntegerProperty property) {
        Checks.isNotNull(property, "property");
        if (isDefined(property)) {
            return integerProperties.get(property);
        } else {
            throw new IllegalArgumentException("Undefined property: " + property);
        }
    }

    /**
     * Sets a long property.
     *
     * @param property The property.
     * @param value The property value.
     */
    public void setProperty(LongProperty property,
                            long value) {
        LOGGER.trace("setProperty({}, {})", property, value);
        Checks.isNotNull(property, "property");
        longProperties.put(property, value);
    }

    /**
     * Returns true if a long property is defined.
     *
     * @param property The property.
     * @return True if property is defined.
     */
    public boolean isDefined(LongProperty property) {
        return longProperties.containsKey(property);
    }

    /**
     * Returns the value of a long property.
     *
     * @param property The property.
     * @return The value of property.
     * @throws IllegalArgumentException When property is not defined.
     */
    public long getProperty(LongProperty property) {
        Checks.isNotNull(property, "property");
        if (isDefined(property)) {
            return longProperties.get(property);
        } else {
            throw new IllegalArgumentException("Undefined property: " + property);
        }
    }

    /**
     * Sets a boolean property.
     *
     * @param property The property.
     * @param value The property value.
     */
    public void setProperty(BooleanProperty property,
                            boolean value) {
        LOGGER.trace("setProperty({}, {})", property, value);
        Checks.isNotNull(property, "property");
        booleanProperties.put(property, value);
        refreshIdentifierNormalizer();
    }

    /**
     * Returns true if a boolean property is defined.
     *
     * @param property The property.
     * @return True if property is defined.
     */
    public boolean isDefined(BooleanProperty property) {
        return booleanProperties.containsKey(property);
    }

    /**
     * Returns the value of a boolean property.
     *
     * @param property The property.
     * @return The value of property.
     * @throws IllegalArgumentException When property is not defined.
     */
    public boolean getProperty(BooleanProperty property) {
        Checks.isNotNull(property, "property");
        if (isDefined(property)) {
            return booleanProperties.get(property);
        } else {
            throw new IllegalArgumentException("Undefined property: " + property);
        }
    }

    public RdbDataType createDataType(String name,
                                      SqlDataType type) {
        return new RdbDataType(name, this, type);
    }

    public RdbTableType createTableType(String name) {
        return new RdbTableType(name, this);
    }

    public RdbTableType getOptionalTableType(String name) {
        return getFirstChild(RdbTableType.class, name);
    }

    public RdbTableType getTableType(String name) {
        return notNull(getOptionalTableType(name), "table type", name);
    }

    public RdbCatalog createCatalog(String name) {
        return new RdbCatalog(name, this);
    }

    public RdbCatalog getOptionalCatalog(String name) {
        return getFirstChild(RdbCatalog.class, name);
    }

    public RdbCatalog getCatalog(String name) {
        return notNull(getOptionalCatalog(name), "catalog", name);
    }

    public RdbCatalog getOrCreateCatalog(String name) {
        RdbCatalog result = getOptionalCatalog(name);
        if (result == null) {
            result = createCatalog(name);
        }
        return result;
    }

    public Iterable<RdbCatalog> getCatalogs() {
        return getChildren(RdbCatalog.class);
    }

    public RdbSchema getFirstSchemaNamed(String name) {
        for (final RdbCatalog catalog : getChildren(RdbCatalog.class)) {
            final RdbSchema schema = catalog.getFirstChild(RdbSchema.class, name);
            if (schema != null) {
                return schema;
            }
        }
        return null;
    }

    public RdbElement getElement(RdbElementPath path) {
        Checks.isNotNull(path, "path");
        if (!getName().equals(path.getPart(RdbElementKind.DATABASE).getName())) {
            return null;
        }
        RdbElement element = this;
        for (int index = 1; index < path.getLength() && element != null; index++) {
            final RdbElementPath.Part part = path.getPart(index);
            element = element.getFirstChild(part.getKind(), part.getName());
        }
        return element;
    }
}