package cdc.rdb;

/**
 * Table description.
 * <p>
 * Its parent is a Schema.<br>
 * Its name must be unique.<br>
 * Its content is:
 * <ul>
 * <li>Table columns
 * <li>Primary key
 * <li>Foreign keys
 * <li>Indices
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class RdbTable extends RdbElement {
    private String tableTypeName;

    RdbTable(String name,
             RdbSchema parent) {
        super(name, parent, false);
    }

    @Override
    public RdbElementKind getKind() {
        return RdbElementKind.TABLE;
    }

    @Override
    public RdbSchema getParent() {
        return (RdbSchema) super.getParent();
    }

    public RdbSchema getSchema() {
        return getParent();
    }

    public RdbCatalog getCatalog() {
        return getSchema().getCatalog();
    }

    public RdbDatabase getDatabase() {
        return getCatalog().getDatabase();
    }

    public void setTableTypeName(String tableTypeName) {
        this.tableTypeName = tableTypeName;
    }

    public String getTableTypeName() {
        return tableTypeName;
    }

    public RdbTableType getTableType() {
        return getDatabase().getOptionalTableType(tableTypeName);
    }

    // Columns

    public RdbTableColumn createColumn(String name) {
        return new RdbTableColumn(name, this);
    }

    public RdbTableColumn getOptionalColumn(String name) {
        return getFirstChild(RdbTableColumn.class, name);
    }

    public RdbTableColumn getColumn(String name) {
        return notNull(getOptionalColumn(name), "table column", name);
    }

    public Iterable<RdbTableColumn> getColumns() {
        return getChildren(RdbTableColumn.class);
    }

    // PK

    public RdbPrimaryKey createPrimaryKey(String name) {
        if (hasChildren(RdbPrimaryKey.class)) {
            throw new IllegalStateException();
        } else {
            return new RdbPrimaryKey(name, this);
        }
    }

    public RdbPrimaryKey getOptionalPrimaryKey() {
        return getFirstChild(RdbPrimaryKey.class);
    }

    public RdbPrimaryKey getPrimaryKey() {
        return notNull(getOptionalPrimaryKey(), "primary key", "");
    }

    public RdbPrimaryKey getOrCreatePrimaryKey(String name) {
        RdbPrimaryKey result = getOptionalPrimaryKey();
        if (result == null) {
            result = createPrimaryKey(name);
        }
        return result;
    }

    // FK

    public RdbForeignKey createForeignKey(String name) {
        return new RdbForeignKey(name, this);
    }

    public RdbForeignKey getOptionalForeignKey(String name) {
        return getFirstChild(RdbForeignKey.class, name);
    }

    public RdbForeignKey getForeignKey(String name) {
        return notNull(getOptionalForeignKey(name), "foreign key", name);
    }

    public RdbForeignKey getOrCreateForeignKey(String name) {
        RdbForeignKey result = getOptionalForeignKey(name);
        if (result == null) {
            result = createForeignKey(name);
        }
        return result;
    }

    public Iterable<RdbForeignKey> getForeignKeys() {
        return getChildren(RdbForeignKey.class);
    }

    // Indices

    public RdbIndex createIndex(String name) {
        return new RdbIndex(name, this);
    }

    public RdbIndex getOptionalIndex(String name) {
        return getFirstChild(RdbIndex.class, name);
    }

    public RdbIndex getIndex(String name) {
        return notNull(getOptionalIndex(name), "index", name);
    }

    public RdbIndex getOrCreateIndex(String name) {
        RdbIndex result = getOptionalIndex(name);
        if (result == null) {
            result = createIndex(name);
        }
        return result;
    }

    public Iterable<RdbIndex> getIndices() {
        return getChildren(RdbIndex.class);
    }
}