package cdc.rdb;

/**
 * Foreign key column description.
 * <p>
 * Its parent is a Foreign key.<br>
 * Its name must be unique.<br>
 * Its is a leaf.
 *
 * @author Damien Carbonne
 *
 */
public final class RdbForeignKeyColumn extends RdbElement {
    private short ordinal;
    private String refColumnName;

    protected RdbForeignKeyColumn(String name,
                                  RdbForeignKey parent) {
        super(name, parent, false);
    }

    @Override
    public RdbElementKind getKind() {
        return RdbElementKind.FOREIGN_KEY_COLUMN;
    }

    @Override
    public RdbForeignKey getParent() {
        return (RdbForeignKey) super.getParent();
    }

    public short getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(short ordinal) {
        this.ordinal = ordinal;
    }

    public String getRefColumnName() {
        return refColumnName;
    }

    public void setRefColumnName(String refColumnName) {
        this.refColumnName = refColumnName;
    }

    public RdbPrimaryKeyColumn getRefColumn() {
        final RdbTable refTable = getParent().getRefTable();
        return refTable == null ? null : refTable.getOptionalPrimaryKey().getOptionalColumn(refColumnName);
    }
}