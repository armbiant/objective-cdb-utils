package cdc.rdb;

import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.Checks;
import cdc.util.strings.StringUtils;

/**
 * Helper class for DB handling.
 *
 * @author Damien Carbonne
 */
public class RdbHelper {
    private static final Logger LOGGER = LogManager.getLogger(RdbHelper.class);
    private final String catalogSeparator;
    private final String identifierQuoteString;
    private static final String TABLE_NAME = "tableName";

    public RdbHelper(DatabaseMetaData metadata) throws SQLException {
        this.catalogSeparator = metadata.getCatalogSeparator();
        this.identifierQuoteString = metadata.getIdentifierQuoteString();
    }

    public RdbHelper(Connection connection) throws SQLException {
        this(connection.getMetaData());
    }

    /**
     * @return The catalog separator.
     */
    public String getCatalogSeparator() {
        return catalogSeparator;
    }

    /**
     * @return The identifier quote string.
     */
    public String getIdentifierQuoteString() {
        return identifierQuoteString;
    }

    /**
     * @param id The naked identifier.
     * @return {@code id} wrapped with identifier quote string.
     */
    public String wrapIdentifier(String id) {
        return identifierQuoteString + id + identifierQuoteString;
    }

    /**
     * @param schemaName The schema name.
     * @param tableName The table name.
     * @return The String designating the table identified by {@code schemaName} and {@code tableName}.
     */
    public String getQueryTableName(String schemaName,
                                    String tableName) {
        Checks.isNotNull(tableName, TABLE_NAME);

        if (StringUtils.isNullOrEmpty(schemaName)) {
            return wrapIdentifier(tableName);
        } else {
            if (StringUtils.isNullOrEmpty(catalogSeparator)) {
                return wrapIdentifier(schemaName) + "." + wrapIdentifier(tableName);
            } else {
                // Should we do that ?
                return wrapIdentifier(schemaName) + catalogSeparator + wrapIdentifier(tableName);
            }
        }
    }

    /**
     * @param schemaName The schema name.
     * @param tableName The table name.
     * @return {@code SELECT * FROM TTT}<br>
     *         where {@code TTT} is the table name.
     */
    public String getSelectClause(String schemaName,
                                  String tableName) {
        Checks.isNotNull(tableName, TABLE_NAME);

        return "SELECT * FROM " + getQueryTableName(schemaName, tableName);
    }

    /**
     * @param schemaName The schema name.
     * @param tableName The table name.
     * @return {@code SELECT COUNT (*) FROM TTT}<br>
     *         where {@code TTT} is the table name.
     */
    public String getTableSizeQuery(String schemaName,
                                    String tableName) {
        Checks.isNotNull(tableName, TABLE_NAME);

        return "SELECT COUNT(*) FROM " + getQueryTableName(schemaName, tableName);
    }

    /**
     *
     * @param schemaName The schema name.
     * @param tableName The table name.
     * @param columns The number of columns.
     * @return {@code INSERT INTO TTT VALUES(?,?,...)}<br>
     *         where {@code TTT} is the table name.
     */
    public String getInsertIntoTableQuery(String schemaName,
                                          String tableName,
                                          int columns) {
        Checks.isNotNull(tableName, TABLE_NAME);

        final StringBuilder builder = new StringBuilder();
        builder.append("INSERT INTO ")
               .append(getQueryTableName(schemaName, tableName))
               .append(" VALUES (");
        for (int index = 0; index < columns; index++) {
            if (index > 0) {
                builder.append(',');
            }
            builder.append('?');
        }
        builder.append(')');
        LOGGER.info("Update query: {}", builder);
        return builder.toString();
    }

    /**
     * @param schemaName The schema name.
     * @param tableName The table name.
     * @param columnsNames The columns names.
     * @return {@code INSERT INTO TTT (N1, N2, ...) VALUES (?,?,...)}<br>
     *         where {@code TTT} is the table name<br>
     *         and {@code Ni} is the i-th column name.
     */
    public String getInsertIntoTableQuery(String schemaName,
                                          String tableName,
                                          List<String> columnsNames) {
        Checks.isNotNull(tableName, TABLE_NAME);
        Checks.isNotNullOrEmpty(columnsNames, "columnsNames");

        final StringBuilder builder = new StringBuilder();
        builder.append("INSERT INTO ")
               .append(getQueryTableName(schemaName, tableName))
               .append(" (");
        for (int index = 0; index < columnsNames.size(); index++) {
            if (index > 0) {
                builder.append(", ");
            }
            builder.append(wrapIdentifier(columnsNames.get(index)));
        }
        builder.append(") VALUES (");
        for (int index = 0; index < columnsNames.size(); index++) {
            if (index > 0) {
                builder.append(',');
            }
            builder.append('?');
        }
        builder.append(')');
        LOGGER.info("Insert query: {}", builder);
        return builder.toString();
    }

    public String getDeleteFromTableQuery(String schemaName,
                                          String tableName,
                                          List<String> whereColumnsNames) {
        Checks.isNotNull(tableName, TABLE_NAME);
        Checks.isNotNullOrEmpty(whereColumnsNames, "whereColumnsNames");

        final StringBuilder builder = new StringBuilder();
        builder.append("DELETE FROM ")
               .append(getQueryTableName(schemaName, tableName))
               .append(" WHERE ");
        for (int index = 0; index < whereColumnsNames.size(); index++) {
            if (index > 0) {
                builder.append(" AND ");
            }
            builder.append(wrapIdentifier(whereColumnsNames.get(index)))
                   .append("=?");
        }
        LOGGER.info("Delete query: {}", builder);
        return builder.toString();
    }

    public String getUpdateTableQuery(String schemaName,
                                      String tableName,
                                      List<String> setColumnsNames,
                                      List<String> whereColumnsNames) {
        Checks.isNotNull(tableName, TABLE_NAME);
        Checks.isNotNullOrEmpty(setColumnsNames, "setColumnsNames");
        Checks.isNotNullOrEmpty(whereColumnsNames, "whereColumnsNames");

        final StringBuilder builder = new StringBuilder();
        builder.append("UPDATE ")
               .append(getQueryTableName(schemaName, tableName))
               .append(" SET");
        for (int index = 0; index < setColumnsNames.size(); index++) {
            if (index > 0) {
                builder.append(',');
            }
            builder.append(' ')
                   .append(wrapIdentifier(setColumnsNames.get(index)))
                   .append("=?");
        }

        builder.append(" WHERE ");
        for (int index = 0; index < whereColumnsNames.size(); index++) {
            if (index > 0) {
                builder.append(" AND ");
            }
            builder.append(wrapIdentifier(whereColumnsNames.get(index)))
                   .append("=?");
        }
        LOGGER.info("Update query: {}", builder);
        return builder.toString();
    }

    /**
     *
     * @param connection The connection.
     * @param schemaName The schema name.
     * @param tableName The table name.
     * @return The size of the table designated by {@code schemaName} and {@cod tableName}.
     * @throws SQLException When an SQL error occurs.
     */
    public long getTableSize(Connection connection,
                             String schemaName,
                             String tableName) throws SQLException {
        final String query = getTableSizeQuery(schemaName, tableName);
        try (final Statement statement = connection.createStatement();
                final ResultSet rs = statement.executeQuery(query)) {
            rs.next();
            return rs.getLong(1);
        }
    }

    /**
     * @param sortings The columns sortings.
     * @return A String defining an order clause based on {@code sortings}.
     */
    public String getOrderClause(List<RdbColumnSorting> sortings) {
        if (sortings.isEmpty()) {
            return "";
        } else {
            final StringBuilder builder = new StringBuilder();
            builder.append(" ORDER BY");
            boolean first = true;
            for (final RdbColumnSorting sorting : sortings) {
                if (first) {
                    first = false;
                } else {
                    builder.append(',');
                }
                builder.append(' ');
                builder.append(identifierQuoteString);
                builder.append(sorting.getColumnName());
                builder.append(identifierQuoteString);
                builder.append(' ');
                builder.append(sorting.getOrder() == RdbColumnOrder.ASCENDING ? "ASC" : "DESC");
            }
            return builder.toString();
        }
    }

    public static void print(ResultSet rs,
                             PrintStream out) {
        try {
            final ResultSetMetaData meta = rs.getMetaData();
            while (rs.next()) {
                for (int column = 1; column <= meta.getColumnCount(); column++) {
                    if (column > 1) {
                        out.print(' ');
                    }
                    out.print(meta.getSchemaName(column));
                    out.print('/');
                    out.print(meta.getTableName(column));
                    out.print('/');
                    out.print(meta.getColumnName(column));
                    out.print(':');
                    out.print(rs.getObject(column));
                }
                out.println();
            }
        } catch (final SQLException e) {
            LOGGER.error(e);
        }
    }
}