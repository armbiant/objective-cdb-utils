package cdc.rdb;

/**
 * Enumeration of element kinds.
 *
 * <pre>
 * Database
 *   Data Types
 *   Table Types
 *   Catalogs
 *     Schemas
 *       User Data Types
 *         Attributes
 *       Functions
 *          Function columns
 *       Procedures
 *          Procedure columns
 *       Tables
 *         Table Columns
 *         Primary Keys
 *           Primary Key Columns
 *         Foreign Keys
 *           Foreign Key Columns
 *         Indices
 *            Index columns
 * </pre>
 *
 * @author Damien Carbonne
 *
 */
public enum RdbElementKind {
    DATABASE,
    CATALOG,
    SCHEMA,
    DATA_TYPE,
    USER_DATA_TYPE,
    ATTRIBUTE,
    FUNCTION,
    FUNCTION_COLUMN,
    PROCEDURE,
    PROCEDURE_COLUMN,
    TABLE_TYPE,
    TABLE,
    TABLE_COLUMN,
    PRIMARY_KEY,
    PRIMARY_KEY_COLUMN,
    FOREIGN_KEY,
    FOREIGN_KEY_COLUMN,
    INDEX,
    INDEX_COLUMN;

    public RdbElementKind getParent() {
        switch (this) {
        case DATABASE:
            return null;

        case DATA_TYPE:
        case TABLE_TYPE:
        case CATALOG:
            return DATABASE;

        case SCHEMA:
            return CATALOG;

        case USER_DATA_TYPE:
        case FUNCTION:
        case PROCEDURE:
        case TABLE:
            return SCHEMA;

        case ATTRIBUTE:
            return USER_DATA_TYPE;

        case FUNCTION_COLUMN:
            return FUNCTION;

        case PROCEDURE_COLUMN:
            return PROCEDURE;

        case FOREIGN_KEY:
        case INDEX:
        case PRIMARY_KEY:
        case TABLE_COLUMN:
            return TABLE;

        case FOREIGN_KEY_COLUMN:
            return FOREIGN_KEY;

        case INDEX_COLUMN:
            return INDEX;

        case PRIMARY_KEY_COLUMN:
            return PRIMARY_KEY;

        default:
            return null;
        }
    }

    public int getDepth() {
        int result = 0;
        RdbElementKind kind = this;
        while (kind != null) {
            result++;
            kind = kind.getParent();
        }
        return result;
    }
}