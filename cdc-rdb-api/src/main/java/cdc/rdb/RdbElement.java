package cdc.rdb;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.function.IterableUtils;
import cdc.util.lang.NotFoundException;
import cdc.util.lang.Operators;

/**
 * Base abstract class of elements.
 *
 * @author Damien Carbonne
 *
 */
public abstract class RdbElement {
    private static final Logger LOGGER = LogManager.getLogger(RdbElement.class);
    private final String name;
    private final RdbElement parent;
    private String comments;
    private final List<RdbElement> children = new ArrayList<>();
    private final RdbElementPath path;
    private static final Map<RdbElementKind, Class<? extends RdbElement>> KIND_TO_CLASS = new EnumMap<>(RdbElementKind.class);
    static {
        KIND_TO_CLASS.put(RdbElementKind.ATTRIBUTE, RdbAttribute.class);
        KIND_TO_CLASS.put(RdbElementKind.CATALOG, RdbCatalog.class);
        KIND_TO_CLASS.put(RdbElementKind.DATA_TYPE, RdbDataType.class);
        KIND_TO_CLASS.put(RdbElementKind.DATABASE, RdbDatabase.class);
        KIND_TO_CLASS.put(RdbElementKind.FOREIGN_KEY, RdbForeignKey.class);
        KIND_TO_CLASS.put(RdbElementKind.FOREIGN_KEY_COLUMN, RdbForeignKeyColumn.class);
        KIND_TO_CLASS.put(RdbElementKind.FUNCTION, RdbFunction.class);
        KIND_TO_CLASS.put(RdbElementKind.FUNCTION_COLUMN, RdbFunctionColumn.class);
        KIND_TO_CLASS.put(RdbElementKind.INDEX, RdbIndex.class);
        KIND_TO_CLASS.put(RdbElementKind.INDEX_COLUMN, RdbIndexColumn.class);
        KIND_TO_CLASS.put(RdbElementKind.PRIMARY_KEY, RdbPrimaryKey.class);
        KIND_TO_CLASS.put(RdbElementKind.PRIMARY_KEY_COLUMN, RdbPrimaryKeyColumn.class);
        KIND_TO_CLASS.put(RdbElementKind.PROCEDURE, RdbProcedure.class);
        KIND_TO_CLASS.put(RdbElementKind.PROCEDURE_COLUMN, RdbProcedureColumn.class);
        KIND_TO_CLASS.put(RdbElementKind.SCHEMA, RdbSchema.class);
        KIND_TO_CLASS.put(RdbElementKind.TABLE, RdbTable.class);
        KIND_TO_CLASS.put(RdbElementKind.TABLE_COLUMN, RdbTableColumn.class);
        KIND_TO_CLASS.put(RdbElementKind.TABLE_TYPE, RdbTableType.class);
        KIND_TO_CLASS.put(RdbElementKind.USER_DATA_TYPE, RdbUserDataType.class);
    }

    protected RdbElement(String name,
                         RdbElement parent,
                         boolean allowDuplicateNames) {
        LOGGER.trace("<init>({})", name);
        this.name = validate(name);
        this.parent = parent;
        if (parent != null) {
            if (!allowDuplicateNames && parent.hasChildren(getClass(), name)) {
                throw new IllegalArgumentException("A sibling named '" + name + "' and type " + getClass().getSimpleName()
                        + " already exists");
            }
            parent.children.add(this);
        }
        this.path = new RdbElementPath(this);
    }

    private static String validate(String name) {
        return name == null ? "" : name;
    }

    protected static <E> E notNull(E value,
                                   String type,
                                   String name) {
        if (value == null) {
            throw new NotFoundException("Could not find " + type + " '" + name + "'");
        } else {
            return value;
        }
    }

    public abstract RdbElementKind getKind();

    public final String getName() {
        return name;
    }

    public final RdbElementPath getPath() {
        return path;
    }

    public RdbElement getParent() {
        return parent;
    }

    public final int getDepth() {
        int result = 0;
        RdbElement index = this;
        while (index != null) {
            index = index.getParent();
            result++;
        }
        return result;
    }

    public final String getComments() {
        return comments;
    }

    public final void setComments(String comments) {
        this.comments = comments;
    }

    protected final <T extends RdbElement> T getParent(Class<T> klass) {
        return klass.cast(parent);
    }

    public final Iterable<RdbElement> getChildren() {
        return children;
    }

    public final <T extends RdbElement> Iterable<T> getChildren(Class<T> cls) {
        return IterableUtils.convert(cls, children);
    }

    public final Iterable<? extends RdbElement> getChildren(RdbElementKind kind) {
        return IterableUtils.filter(children, e -> e.getKind() == kind);
    }

    public final <T extends RdbElement> int getChildrenCount(Class<T> klass) {
        return IterableUtils.size(getChildren(klass));
    }

    public final int getChildrenCount(RdbElementKind kind) {
        return IterableUtils.size(getChildren(kind));
    }

    public final <T extends RdbElement> boolean hasChildren(Class<T> klass) {
        return !IterableUtils.isEmpty(getChildren(klass));
    }

    public final boolean hasChildren(RdbElementKind kind) {
        return !IterableUtils.isEmpty(getChildren(kind));
    }

    public final <T extends RdbElement> T getFirstChild(Class<T> klass,
                                                        String name) {
        final String n = validate(name);
        for (final T child : getChildren(klass)) {
            if (Operators.equals(n, child.getName())) {
                return child;
            }
        }
        return null;
    }

    public final RdbElement getFirstChild(RdbElementKind kind,
                                          String name) {
        return getFirstChild(KIND_TO_CLASS.get(kind), name);
    }

    public final <T extends RdbElement> int getChildrenCount(Class<T> klass,
                                                             String name) {
        final String n = validate(name);
        int result = 0;
        for (final T child : getChildren(klass)) {
            if (Operators.equals(n, child.getName())) {
                result++;
            }
        }
        return result;
    }

    public final int getChildrenCount(RdbElementKind kind,
                                      String name) {
        return getChildrenCount(KIND_TO_CLASS.get(kind), name);
    }

    public final <T extends RdbElement> List<T> getChildren(Class<T> klass,
                                                            String name) {
        final String n = validate(name);
        final List<T> result = new ArrayList<>();
        for (final T child : getChildren(klass)) {
            if (Operators.equals(n, child.getName())) {
                result.add(child);
            }
        }
        return result;
    }

    public final List<RdbElement> getChildren(RdbElementKind kind,
                                              String name) {
        final String n = validate(name);
        final List<RdbElement> result = new ArrayList<>();
        for (final RdbElement child : getChildren(kind)) {
            if (Operators.equals(n, child.getName())) {
                result.add(child);
            }
        }
        return result;
    }

    public final <T extends RdbElement> T getFirstChild(Class<T> klass) {
        final Iterable<T> tmp = getChildren(klass);
        final Iterator<T> iter = tmp.iterator();
        if (iter.hasNext()) {
            return iter.next();
        } else {
            return null;
        }
    }

    public final RdbElement getFirstChild(RdbElementKind kind) {
        return getFirstChild(KIND_TO_CLASS.get(kind));
    }

    public final <T extends RdbElement> boolean hasChildren(Class<T> klass,
                                                            String name) {
        return getFirstChild(klass, name) != null;
    }

    public final boolean hasChildren(RdbElementKind kind,
                                     String name) {
        return hasChildren(KIND_TO_CLASS.get(kind), name);
    }

    @Override
    public String toString() {
        return getKind() + " '" + getName() + "'";
    }
}