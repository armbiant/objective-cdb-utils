package cdc.rdb;

/**
 * Index column description.
 * <p>
 * Its parent is an Index.<br>
 * Its name must be unique.<br>
 * Its is a leaf.
 *
 * @author Damien Carbonne
 *
 */
public final class RdbIndexColumn extends RdbElement {
    private short ordinal = -1;
    private String columnName;

    protected RdbIndexColumn(String name,
                             RdbIndex parent) {
        super(name, parent, false);
    }

    @Override
    public RdbElementKind getKind() {
        return RdbElementKind.INDEX_COLUMN;
    }

    @Override
    public RdbIndex getParent() {
        return (RdbIndex) super.getParent();
    }

    public RdbIndex getIndex() {
        return getParent();
    }

    public short getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(short ordinal) {
        this.ordinal = ordinal;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public RdbTableColumn getColumn() {
        final RdbTable table = getParent().getParent();
        return table == null ? null : table.getOptionalColumn(columnName);
    }
}