package cdc.rdb;

/**
 * Catalog description.
 * <p>
 * Its parent is a Database.<br>
 * Its name must be unique.<br>
 * Its content is:
 * <ul>
 * <li>Schemas
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class RdbCatalog extends RdbElement {
    RdbCatalog(String name,
               RdbDatabase parent) {
        super(name, parent, false);
    }

    @Override
    public final RdbElementKind getKind() {
        return RdbElementKind.CATALOG;
    }

    @Override
    public RdbDatabase getParent() {
        return getParent(RdbDatabase.class);
    }

    public RdbDatabase getDatabase() {
        return getParent();
    }

    public RdbSchema createSchema(String name) {
        return new RdbSchema(name, this);
    }

    public RdbSchema getOptionalSchema(String name) {
        return getFirstChild(RdbSchema.class, name);
    }

    public RdbSchema getSchema(String name) {
        return notNull(getOptionalSchema(name), "schema", name);
    }

    public RdbSchema getOrCreateSchema(String name) {
        RdbSchema result = getOptionalSchema(name);
        if (result == null) {
            result = createSchema(name);
        }
        return result;
    }

    public Iterable<RdbSchema> getSchemas() {
        return getChildren(RdbSchema.class);
    }
}