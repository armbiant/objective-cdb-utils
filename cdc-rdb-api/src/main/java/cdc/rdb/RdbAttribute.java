package cdc.rdb;

/**
 * Attribute description.
 * <p>
 * Its parent is a User data type.<br>
 * Its name must be unique.(?)<br>
 * Its is a leaf.
 *
 * @author Damien Carbonne
 *
 */
public final class RdbAttribute extends RdbElement {
    // TODO attributes
    protected RdbAttribute(String name,
                           RdbUserDataType parent) {
        super(name, parent, false);
    }

    @Override
    public RdbElementKind getKind() {
        return RdbElementKind.ATTRIBUTE;
    }

    @Override
    public RdbUserDataType getParent() {
        return (RdbUserDataType) super.getParent();
    }

    public RdbUserDataType getUserDataType() {
        return getParent();
    }
}