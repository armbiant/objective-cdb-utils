package cdc.rdb;

/**
 * Procedure column description.
 * <p>
 * Its parent is a Procedure.<br>
 * Its name must be unique. (? A duplicate has been found in PostgreSQL)<br>
 * Its is a leaf.
 *
 * @author Damien Carbonne
 *
 */
public final class RdbProcedureColumn extends RdbElement {
    // TODO attributes
    protected RdbProcedureColumn(String name,
                                 RdbProcedure parent) {
        super(name, parent, false);
    }

    @Override
    public RdbElementKind getKind() {
        return RdbElementKind.PROCEDURE_COLUMN;
    }

    @Override
    public RdbProcedure getParent() {
        return (RdbProcedure) super.getParent();
    }

    public RdbProcedure getProcedure() {
        return getParent();
    }
}