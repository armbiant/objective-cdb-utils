package cdc.rdb;

/**
 * Function description.
 * <p>
 * Its parent is a Schema.<br>
 * Its name must NOT be unique.<br>
 * Its content is:
 * <ul>
 * <li>Function columns
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class RdbFunction extends RdbElement {
    private String specificName;
    private FunctionResultType resultType;

    RdbFunction(String name,
                RdbSchema parent) {
        super(name, parent, true);
    }

    @Override
    public final RdbElementKind getKind() {
        return RdbElementKind.FUNCTION;
    }

    @Override
    public RdbSchema getParent() {
        return getParent(RdbSchema.class);
    }

    public RdbFunctionColumn createColumn(String name) {
        return new RdbFunctionColumn(name, this);
    }

    public Iterable<RdbFunctionColumn> getColumns() {
        return getChildren(RdbFunctionColumn.class);
    }

    public RdbFunctionColumn getOptionalColumn(String name) {
        return getFirstChild(RdbFunctionColumn.class, name);
    }

    public RdbFunctionColumn getColumn(String name) {
        return notNull(getOptionalColumn(name), "function column", name);
    }

    public String getSpecificName() {
        return specificName;
    }

    public void setSpecificName(String specificName) {
        this.specificName = specificName;
    }

    public FunctionResultType getResultType() {
        return resultType;
    }

    public void setResultType(FunctionResultType resultType) {
        this.resultType = resultType;
    }
}