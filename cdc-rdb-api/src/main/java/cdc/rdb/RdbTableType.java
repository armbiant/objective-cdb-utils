package cdc.rdb;

/**
 * Table description.
 * <p>
 * Its parent is a Database.<br>
 * Its name must be unique.<br>
 * Its is a leaf.
 *
 * @author Damien Carbonne
 *
 */
public class RdbTableType extends RdbElement {
    RdbTableType(String name,
                 RdbDatabase parent) {
        super(name, parent, false);
    }

    @Override
    public final RdbElementKind getKind() {
        return RdbElementKind.TABLE_TYPE;
    }

    @Override
    public RdbDatabase getParent() {
        return getParent(RdbDatabase.class);
    }
}