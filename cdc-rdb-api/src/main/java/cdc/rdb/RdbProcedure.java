package cdc.rdb;

/**
 * Procedure description.
 * <p>
 * Its parent is a Schema.<br>
 * Its name must NOT be unique.<br>
 * Its content is:
 * <ul>
 * <li>Procedure columns
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class RdbProcedure extends RdbElement {
    private String specificName;
    private ProcedureResultType resultType;

    RdbProcedure(String name,
                 RdbSchema parent) {
        super(name, parent, true);
    }

    @Override
    public final RdbElementKind getKind() {
        return RdbElementKind.PROCEDURE;
    }

    @Override
    public RdbSchema getParent() {
        return getParent(RdbSchema.class);
    }

    public RdbProcedureColumn createColumn(String name) {
        return new RdbProcedureColumn(name, this);
    }

    public Iterable<RdbProcedureColumn> getColumns() {
        return getChildren(RdbProcedureColumn.class);
    }

    public RdbProcedureColumn getOptionalColumn(String name) {
        return getFirstChild(RdbProcedureColumn.class, name);
    }

    public RdbProcedureColumn getColumn(String name) {
        return notNull(getOptionalColumn(name), "procedure column", name);
    }

    public String getSpecificName() {
        return specificName;
    }

    public void setSpecificName(String specificName) {
        this.specificName = specificName;
    }

    public ProcedureResultType getResultType() {
        return resultType;
    }

    public void setResultType(ProcedureResultType resultType) {
        this.resultType = resultType;
    }
}