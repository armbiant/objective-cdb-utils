package cdc.rdb;

/**
 * Primary key column description.
 * <p>
 * Its parent is a Primary key.<br>
 * Its name must be unique.<br>
 * Its is a leaf.
 *
 * @author Damien Carbonne
 *
 */
public final class RdbPrimaryKeyColumn extends RdbElement {
    private short ordinal;

    protected RdbPrimaryKeyColumn(String name,
                                  RdbPrimaryKey parent) {
        super(name, parent, false);
    }

    @Override
    public RdbElementKind getKind() {
        return RdbElementKind.PRIMARY_KEY_COLUMN;
    }

    @Override
    public RdbPrimaryKey getParent() {
        return (RdbPrimaryKey) super.getParent();
    }

    public short getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(short ordinal) {
        this.ordinal = ordinal;
    }

    public RdbTableColumn getColumn() {
        return getParent().getParent().getOptionalColumn(getName());
    }
}