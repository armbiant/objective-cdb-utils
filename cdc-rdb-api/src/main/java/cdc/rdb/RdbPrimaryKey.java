package cdc.rdb;

/**
 * Primary key description.
 * <p>
 * Its parent is a Table.<br>
 * Its name must be unique.<br>
 * Its content is:
 * <ul>
 * <li>Primary key columns
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class RdbPrimaryKey extends RdbElement {
    protected RdbPrimaryKey(String name,
                            RdbTable parent) {
        super(name, parent, false);
    }

    @Override
    public RdbElementKind getKind() {
        return RdbElementKind.PRIMARY_KEY;
    }

    @Override
    public RdbTable getParent() {
        return (RdbTable) super.getParent();
    }

    public RdbPrimaryKeyColumn createColumn(String name) {
        return new RdbPrimaryKeyColumn(name, this);
    }

    public RdbPrimaryKeyColumn getOptionalColumn(String name) {
        return getFirstChild(RdbPrimaryKeyColumn.class, name);
    }

    public RdbPrimaryKeyColumn getColumn(String name) {
        return notNull(getOptionalColumn(name), "primary key column", name);
    }

    public Iterable<RdbPrimaryKeyColumn> getColumns() {
        return getChildren(RdbPrimaryKeyColumn.class);
    }
}