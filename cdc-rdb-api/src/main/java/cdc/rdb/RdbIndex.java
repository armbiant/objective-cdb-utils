package cdc.rdb;

/**
 * Index description.
 * <p>
 * Its parent is a Table.<br>
 * Its name must be unique.<br>
 * Its content is:
 * <ul>
 * <li>Index columns
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class RdbIndex extends RdbElement {
    private RdbIndexType type;

    protected RdbIndex(String name,
                       RdbTable parent) {
        super(name, parent, false);
    }

    @Override
    public RdbElementKind getKind() {
        return RdbElementKind.INDEX;
    }

    @Override
    public RdbTable getParent() {
        return (RdbTable) super.getParent();
    }

    public RdbTable getTable() {
        return getParent();
    }

    public RdbIndexColumn createColumn(String name) {
        return new RdbIndexColumn(name, this);
    }

    public Iterable<RdbIndexColumn> getColumns() {
        return getChildren(RdbIndexColumn.class);
    }

    public RdbIndexColumn getOptionalColumn(String name) {
        return getFirstChild(RdbIndexColumn.class, name);
    }

    public RdbIndexColumn getColumn(String name) {
        return notNull(getOptionalColumn(name), "index column", name);
    }

    public RdbIndexType getType() {
        return type;
    }

    public void setType(RdbIndexType type) {
        this.type = type;
    }
}