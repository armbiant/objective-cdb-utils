package cdc.rdb;

/**
 * Foreign key description.
 * <p>
 * Its parent is a Table.<br>
 * Its name must be unique.<br>
 * Its content is:
 * <ul>
 * <li>Foreign key columns
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public class RdbForeignKey extends RdbElement {
    private String refCatalogName;
    private String refSchemaName;
    private String refTableName;

    protected RdbForeignKey(String name,
                            RdbTable parent) {
        super(name, parent, false);
    }

    @Override
    public RdbElementKind getKind() {
        return RdbElementKind.FOREIGN_KEY;
    }

    @Override
    public RdbTable getParent() {
        return (RdbTable) super.getParent();
    }

    public RdbForeignKeyColumn createColumn(String name) {
        return new RdbForeignKeyColumn(name, this);
    }

    public Iterable<RdbForeignKeyColumn> getColumns() {
        return getChildren(RdbForeignKeyColumn.class);
    }

    public String getRefCatalogName() {
        return refCatalogName;
    }

    public RdbCatalog getRefCatalog() {
        return getParent().getDatabase().getOptionalCatalog(refCatalogName);
    }

    public void setRefCatalogName(String refCatalogName) {
        this.refCatalogName = refCatalogName;
    }

    public String getRefSchemaName() {
        return refSchemaName;
    }

    public RdbSchema getRefSchema() {
        final RdbCatalog refCatalog = getRefCatalog();
        return refCatalog == null ? null : refCatalog.getOptionalSchema(refSchemaName);
    }

    public void setRefSchemaName(String refSchemaName) {
        this.refSchemaName = refSchemaName;
    }

    public String getRefTableName() {
        return refTableName;
    }

    public RdbTable getRefTable() {
        final RdbSchema refSchema = getRefSchema();
        return refSchema == null ? null : refSchema.getOptionalTable(refTableName);
    }

    public void setRefTableName(String refTableName) {
        this.refTableName = refTableName;
    }
}