package cdc.rdb;

import java.sql.DatabaseMetaData;

public enum YesNoUnknown {
    YES,
    NO,
    UNKNOWN;

    public static YesNoUnknown decode(String code) {
        if ("YES".equals(code)) {
            return YES;
        } else if ("NO".equals(code)) {
            return NO;
        } else {
            return UNKNOWN;
        }
    }

    public static YesNoUnknown decode(short code) {
        switch (code) {
        case DatabaseMetaData.typeNoNulls:
            return YesNoUnknown.NO;
        case DatabaseMetaData.typeNullable:
            return YES;
        case DatabaseMetaData.typeNullableUnknown:
            return YesNoUnknown.UNKNOWN;
        default:
            return null;
        }
    }
}