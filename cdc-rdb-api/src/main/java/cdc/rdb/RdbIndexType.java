package cdc.rdb;

import java.sql.DatabaseMetaData;
import java.util.HashMap;
import java.util.Map;

public enum RdbIndexType {
    INDEX_STATISTIC(DatabaseMetaData.tableIndexStatistic),
    INDEX_CLUSTERED(DatabaseMetaData.tableIndexClustered),
    INDEX_HASHED(DatabaseMetaData.tableIndexHashed),
    INDEX_OTHER(DatabaseMetaData.tableIndexOther);

    private static final Map<Short, RdbIndexType> MAP = new HashMap<>();
    static {
        for (final RdbIndexType value : RdbIndexType.values()) {
            MAP.put(value.getCode(), value);
        }
    }

    private final short code;

    private RdbIndexType(short code) {
        this.code = code;
    }

    public short getCode() {
        return code;
    }

    public static RdbIndexType decode(short code) {
        return MAP.get(code);
    }
}