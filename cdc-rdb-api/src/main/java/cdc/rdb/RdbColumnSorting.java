package cdc.rdb;

import cdc.util.lang.Checks;

public class RdbColumnSorting {
    private final String columnName;
    private final RdbColumnOrder order;

    public RdbColumnSorting(String columnName,
                            RdbColumnOrder order) {

        this.columnName = Checks.isNotNull(columnName, "columnName");
        this.order = Checks.isNotNull(order, "order");
    }

    public String getColumnName() {
        return columnName;
    }

    public RdbColumnOrder getOrder() {
        return order;
    }

    @Override
    public String toString() {
        return getColumnName() + " " + getOrder();
    }
}