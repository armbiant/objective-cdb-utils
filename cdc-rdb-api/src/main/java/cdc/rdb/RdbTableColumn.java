package cdc.rdb;

/**
 * Table column description.
 * <p>
 * Its parent is a Table.<br>
 * Its name must be unique.<br>
 * Its is a leaf.
 *
 * @author Damien Carbonne
 *
 */
public final class RdbTableColumn extends RdbElement {
    private SqlDataType dataType;
    private String typeName = "";
    private int size = -1;
    private int digits = -1;
    private int radix = -1;
    private YesNoUnknown nullable = YesNoUnknown.UNKNOWN;
    private String defaultValue;
    private int ordinal = -1;
    private YesNoUnknown autoIncrement = YesNoUnknown.UNKNOWN;
    private YesNoUnknown generated = YesNoUnknown.UNKNOWN;

    protected RdbTableColumn(String name,
                             RdbTable parent) {
        super(name, parent, false);
    }

    @Override
    public RdbElementKind getKind() {
        return RdbElementKind.TABLE_COLUMN;
    }

    @Override
    public RdbTable getParent() {
        return (RdbTable) super.getParent();
    }

    public SqlDataType getDataType() {
        return dataType;
    }

    public void setDataType(SqlDataType dataType) {
        this.dataType = dataType;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getDigits() {
        return digits;
    }

    public void setDigits(int digits) {
        this.digits = digits;
    }

    public int getRadix() {
        return radix;
    }

    public void setRadix(int radix) {
        this.radix = radix;
    }

    public YesNoUnknown getNullable() {
        return nullable;
    }

    public void setNullable(YesNoUnknown nullable) {
        this.nullable = nullable;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }

    public YesNoUnknown getAutoIncrement() {
        return autoIncrement;
    }

    public void setAutoIncrement(YesNoUnknown autoIncrement) {
        this.autoIncrement = autoIncrement;
    }

    public YesNoUnknown getGenerated() {
        return generated;
    }

    public void setGenerated(YesNoUnknown generated) {
        this.generated = generated;
    }

    public boolean isPrimaryKey() {
        final RdbTable table = getParent();
        for (final RdbPrimaryKey pk : table.getChildren(RdbPrimaryKey.class)) {
            for (final RdbPrimaryKeyColumn column : pk.getChildren(RdbPrimaryKeyColumn.class)) {
                if (column.getName().equals(getName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isForeignKey() {
        final RdbTable table = getParent();
        for (final RdbForeignKey fk : table.getChildren(RdbForeignKey.class)) {
            for (final RdbForeignKeyColumn column : fk.getChildren(RdbForeignKeyColumn.class)) {
                if (column.getName().equals(getName())) {
                    return true;
                }
            }
        }
        return false;
    }
}