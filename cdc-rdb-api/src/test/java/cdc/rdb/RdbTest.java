package cdc.rdb;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class RdbTest {
    @Test
    void test1() {
        final RdbDatabase db = new RdbDatabase("db");
        final RdbCatalog catalog1 = db.createCatalog("catalog1");
        final RdbSchema schema1 = catalog1.createSchema("schema1");
        final RdbTable table1 = schema1.createTable("table1");
        final RdbTableColumn tableColumn1 = table1.createColumn("column1");
        final RdbTableColumn tableColumn2 = table1.createColumn("column2");

        assertEquals(db, db.getElement(db.getPath()));
        assertEquals(catalog1, db.getElement(catalog1.getPath()));
        assertEquals(schema1, db.getElement(schema1.getPath()));
        assertEquals(table1, db.getElement(table1.getPath()));
        assertEquals(tableColumn1, db.getElement(tableColumn1.getPath()));
        assertEquals(tableColumn2, db.getElement(tableColumn2.getPath()));
    }
}