package cdc.rdb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

class RdbElementPathTest {
    private static void testParts(RdbElementPath path) {
        final Set<RdbElementKind> kinds = new HashSet<>();
        RdbElementKind kind = path.getKind();
        while (kind != null) {
            kinds.add(kind);
            kind = kind.getParent();
        }
        for (final RdbElementKind k : RdbElementKind.values()) {
            assertEquals(kinds.contains(k), path.hasPart(k));
            if (kinds.contains(k)) {
                assertEquals(k, path.getPart(k).getKind());
            } else {
                assertEquals(null, path.getPart(k));
            }
        }
    }

    private static void testConstructor(String s,
                                        RdbElementKind expectedKind) {
        final RdbElementPath path = new RdbElementPath(s);
        assertEquals(expectedKind, path.getKind());
        assertEquals(s, path.toString());
        final RdbElementKind expectedParentKind = expectedKind.getParent();
        final RdbElementPath parent = path.getParent();
        final RdbElementKind parentKind = parent == null ? null : parent.getKind();
        assertEquals(expectedParentKind, parentKind);
        testParts(path);
    }

    @Test
    void testConstructor() {
        testConstructor("DATABASE:db", RdbElementKind.DATABASE);
        testConstructor("DATABASE:", RdbElementKind.DATABASE);

        testConstructor("DATA_TYPE:db/dt", RdbElementKind.DATA_TYPE);
        testConstructor("DATA_TYPE:db/", RdbElementKind.DATA_TYPE);
        testConstructor("DATA_TYPE:/dt", RdbElementKind.DATA_TYPE);
        testConstructor("DATA_TYPE:/", RdbElementKind.DATA_TYPE);

        testConstructor("TABLE_TYPE:db/tt", RdbElementKind.TABLE_TYPE);
        testConstructor("TABLE_TYPE:db/", RdbElementKind.TABLE_TYPE);
        testConstructor("TABLE_TYPE:/tt", RdbElementKind.TABLE_TYPE);
        testConstructor("TABLE_TYPE:/", RdbElementKind.TABLE_TYPE);

        testConstructor("CATALOG:db/cat", RdbElementKind.CATALOG);
        testConstructor("CATALOG:db/", RdbElementKind.CATALOG);
        testConstructor("CATALOG:/cat", RdbElementKind.CATALOG);
        testConstructor("CATALOG:/", RdbElementKind.CATALOG);

        testConstructor("SCHEMA:db/cat/schema", RdbElementKind.SCHEMA);

        testConstructor("USER_DATA_TYPE:db/cat/schema/udt", RdbElementKind.USER_DATA_TYPE);
        testConstructor("FUNCTION:db/cat/schema/fun", RdbElementKind.FUNCTION);
        testConstructor("PROCEDURE:db/cat/schema/proc", RdbElementKind.PROCEDURE);
        testConstructor("TABLE:db/cat/schema/table", RdbElementKind.TABLE);
    }

    @Test
    void testInvalidPath1() {
        assertThrows(IllegalArgumentException.class, () -> {
            new RdbElementPath("ABLE:db/cat/schema/table");
        });
    }

    @Test
    void testInvalidPath2() {
        assertThrows(IllegalArgumentException.class, () -> {
            new RdbElementPath("TABLEdb/cat/schema/table");
        });
    }

    @Test
    void testInvalidPath3() {
        assertThrows(IllegalArgumentException.class, () -> {
            new RdbElementPath("TABLE:db/cat/schema/table/column");
        });
    }
}