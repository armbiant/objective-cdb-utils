package cdc.rdb.demos;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.rdb.RdbDatabase;
import cdc.rdb.RdbDatabaseIo;
import cdc.util.lang.FailureReaction;

public final class RdbXmlDemo {
    private static final Logger LOGGER = LogManager.getLogger(RdbXmlDemo.class);

    private RdbXmlDemo() {
    }

    public static void main(String[] args) throws IOException {
        LOGGER.info("Read database file");
        final RdbDatabaseIo.DataLoader loader = new RdbDatabaseIo.DataLoader(FailureReaction.WARN);
        final RdbDatabase database = loader.loadXml("target/derby.xml");
        LOGGER.info("Write database file");
        RdbDatabaseIo.print(database, new File("target/derby2.xml"));
        LOGGER.info("Done");
    }
}