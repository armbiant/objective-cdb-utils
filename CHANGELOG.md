# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.22.2] - 2023-01-28
### Changed
- Updated dependencies:
    - cdc-kernel-0.21.1
    - cdc-office-0.29.0
    - cdc-util-0.29.0
    - org.junit-5.9.2


## [0.22.1] - 2023-01-02
### Added
- Added detection of non Unicode characters.

### Changed
- Updated dependencies:
    - cdc-kernel-0.21.0
    - cdc-office-0.28.0
    - cdc-util-0.28.2


## [0.22.0] - 2022-11-12
### Added
- Added Builders to `DatabaseConfig`.

### Changed
- Updated dependencies:
    - cdc-kernel-0.20.7
    - cdc-office-0.27.0
    - cdc-util-0.28.1
    - org.apache.log4j-2.19.0
    - org.junit-5.9.1


## [0.21.2] - 2022-08-24
### Changed
- Updated dependencies:
    - cdc-kernel-0.20.6
    - cdc-office-0.24.0
    - cdc-util-0.28.0
    - org.junit-5.9.0


## [0.21.1] - 2022-07-07
### Changed
- Updated dependencies:
    - cdc-kernel-0.20.5
    - cdc-office-0.23.1
    - cdc-util-0.27.0
    - org.apache.log4j-2.18.0
    - org.junit-5.9.0-RC1


## [0.21.0] - 2022-06-18
### Added
- Added new methods to `RdbHelper`.
- Updated dependencies:
    - cdc-kernel-0.20.4
    - cdc-office-0.23.0
    - cdc-util-0.26.0


## [0.20.3] - 2022-05-21
### Changed
- Updated maven plugins
- Updated dependencies:
    - cdc-kernel-0.20.3
    - cdc-office-0.22.0
    - cdc-util-0.25.0


## [0.20.2] - 2022-03-11
### Changed
- Updated dependencies:
    - cdc-kernel-0.20.2
    - cdc-office-0.21.1
    - cdc-util-0.23.0
    - org.apache.log4j-2.17.2
- `Config` data is now retrieved from Manifest.


## [0.20.1] - 2022-02-13
### Changed
- Updated dependencies
    - cdc-kernel-0.20.1
    - cdc-office-0.21.0
    - cdc-util-0.20.0


## [0.20.0] - 2022-02-05
### Changed
- Upgraded to Java 11
- Updated dependencies
    - cdc-kernel-0.20.0
    - cdc-office-0.20.0


## [0.12.3] - 2022-01-29
### Changed
- Updated maven plugins
- Updated dependencies
    - cdc-office-0.15.0


## [0.12.2] - 2022-01-15
### Security
- Updated dependencies:
    - cdc-kernel-0.14.2
    - cdc-office-0.14.2
    - cdc-util-0.14.2
    - org.apache.log4j-2.17.1. #1


## [0.12.1] - 2021-12-28
### Security
- Updated dependencies:
    - cdc-kernel-0.14.1
    - cdc-office-0.14.1
    - cdc-util-0.14.1
    - org.apache.log4j-2.17.0. #1


## [0.12.0] - 2021-12-15
### Security
- Updated dependencies:
    - cdc-kernel-0.14.0
    - cdc-office-0.14.0
    - cdc-util-0.14.0
    - org.apache.log4j-2.16.0. #1

### Fixed
- Fixed warnings.


## [0.11.2] - 2021-10-02
### Changed
- Updated dependencies.


## [0.11.1] - 2021-07-23
### Changed
- Updated dependencies.


## [0.11.0] - 2021-05-10
### Changed
- Moved code from **tools** to **api** module . cdc-java/cdc-impex#28
- Moved code from `cdc.rdb.tools.dump` to `cdc.rdb.tools.dump.config` package.


## [0.10.0] - 2021-05-03
### Added
- First release, extracted from [cdc-utils](https://gitlab.com/cdc-java/cdc-util). cdc-java/cdc-util#39
